//
//  RegisterVC.m
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import "RegisterVC.h"
@import Firebase;
@interface RegisterVC ()

@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self createBorder];
    UIGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
    [self.view addGestureRecognizer:tap];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:@"UIKeyboardWillShowNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:@"UIKeyboardWillHideNotification" object:nil];
    
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    // Save the height of keyboard and animation duration
    if (self.view.frame.origin.y == 0) {
        NSDictionary *userInfo = [notification userInfo];
        CGRect keyboardRect = [userInfo[@"UIKeyboardBoundsUserInfoKey"] CGRectValue];
        float height = keyboardRect.size.height-60;
        self.view.frame = CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - height, self.view.frame.size.width, self.view.frame.size.height);
    }
}
// Reset the desired height
- (void)keyboardWillHide:(NSNotification *)notification
{
    self.view.frame = CGRectMake(self.view.frame.origin.x, 0, self.view.frame.size.width, self.view.frame.size.height);
}

- (IBAction)dismissAction:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)signUp:(id)sender {
    if (_emailField.text != nil && _nameField.text != nil && _surnameField.text != nil && _passwordField.text != nil) {
        if (_passwordField.text.length >= 8) {
            [[FIRAuth auth] createUserWithEmail:_emailField.text password:_passwordField.text completion:^(FIRUser *_Nullable user, NSError * _Nullable error) {
                if (error == nil) {
                    FIRUserProfileChangeRequest *changeRequest = [user profileChangeRequest];
                    changeRequest.displayName = [NSString stringWithFormat:@"%@ %@", _nameField.text, _surnameField.text];
                    [changeRequest commitChangesWithCompletion:^(NSError * _Nullable error){
                        if (error == nil) {
                            [[NSUserDefaults standardUserDefaults] setValue:user.uid forKey:@"uid"];
                            [self performSegueWithIdentifier:@"signupSuccess" sender:nil];
                        } else {
                            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:[error localizedDescription] delegate:self cancelButtonTitle:@"Ок" otherButtonTitles:nil, nil];
                            [alert show];
                        }
                    }];
                } else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:[error localizedDescription] delegate:self cancelButtonTitle:@"Ок" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Пароль должен сожержать 8 или более символов!" delegate:self cancelButtonTitle:@"Ок" otherButtonTitles:nil, nil];
            [alert show];
        }
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Необходимо заполнить все поля!" delegate:self cancelButtonTitle:@"Ок" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void) handleTapFrom: (UITapGestureRecognizer *)recognizer
{
    [[self view] endEditing:YES];
}


-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)createBorder {
    _nameView.layer.borderWidth = 0.5;
    _nameView.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5].CGColor;
    
    _surnameView.layer.borderWidth = 0.5;
    _surnameView.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5].CGColor;
    
    _emailView.layer.borderWidth = 0.5;
    _emailView.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5].CGColor;
    
    _passwordView.layer.borderWidth = 0.5;
    _passwordView.layer.borderColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.5].CGColor;
}

@end
