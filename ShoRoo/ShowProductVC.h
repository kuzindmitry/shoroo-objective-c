//
//  ShowProductVC.h
//  ShoRoo
//
//  Created by Dmitry Kuzin on 11.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Product.h"
#import "TextViewShowProductCell.h"

@interface ShowProductVC : UIViewController <UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIImageView *imageProduct;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property Product *product;


@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *minusButton;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;

@property NSInteger count;
- (IBAction)addToCart:(id)sender;
- (IBAction)minusCount:(id)sender;
- (IBAction)plusCount:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

- (IBAction)changeSegmentedControl:(id)sender;


@end
