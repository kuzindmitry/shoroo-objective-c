//
//  AllCategoriesVC.m
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import "AllGlobalCategoriesVC.h"
#import "Product.h"
#import "ShowRoom.h"
#import "CategoryVC.h"
@import Firebase;



@interface AllGlobalCategoriesVC ()

@end

@implementation AllGlobalCategoriesVC

@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    FIRDatabaseReference *ref = [[[FIRDatabase database] reference] child:@"category"];
    [ref observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nullable snapshot){
        _categories = [[NSMutableDictionary alloc] init];
        for (FIRDataSnapshot *i in snapshot.children) {
            NSMutableDictionary *elements = [[NSMutableDictionary alloc] init];
            for (FIRDataSnapshot *element in [i childSnapshotForPath:@"elements"].children) {
                NSMutableArray *products = [[NSMutableArray alloc] init];
                for (FIRDataSnapshot *currentProduct in [element childSnapshotForPath:@"products"].children) {
                    Product *product = [[Product alloc] init];
                    product.title = [[currentProduct childSnapshotForPath:@"title"] value];
                    product.image = [[currentProduct childSnapshotForPath:@"image"] value];
                    product.productId = [[currentProduct childSnapshotForPath:@"id"] value];
                    product.categoryId = [[element childSnapshotForPath:@"id"] value];
                    product.amount = [[currentProduct childSnapshotForPath:@"amount"] value];
                    product.descr = [[currentProduct childSnapshotForPath:@"description"] value];
                    product.characteristics = [[currentProduct childSnapshotForPath:@"characteristics"] value];
                    ShowRoom * showroom = [[ShowRoom alloc] init];
                    showroom.showroomId = [[[currentProduct childSnapshotForPath:@"showroom"] childSnapshotForPath:@"id"] value];
                    showroom.image = [[[currentProduct childSnapshotForPath:@"showroom"] childSnapshotForPath:@"image"] value];
                    showroom.name = [[[currentProduct childSnapshotForPath:@"showroom"] childSnapshotForPath:@"name"] value];
                    product.showroom = showroom;
                    [products addObject:product];
                }
                if (products.count > 0) {
                    [elements setValue:products forKey:[[element childSnapshotForPath:@"title"] value]];
                }
                //[elements addObject:[[element childSnapshotForPath:@"title"] value]];
            }
            //Product *product = [[Product alloc] init];
            if (elements.count > 0) {
                [_categories setValue:elements forKeyPath:[[i childSnapshotForPath:@"title"] value]];
            }
            
        }
        [self.tableView reloadData];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _categories.count;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return _categories.allKeys[section];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSDictionary *values = [_categories valueForKey:_categories.allKeys[section]];
    return values.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GlobalCategoryCell" forIndexPath:indexPath];
    NSDictionary *values = [_categories valueForKey:_categories.allKeys[indexPath.section]];
    cell.textLabel.text = values.allKeys[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //FIRDatabaseReference *ref = [[FIRDatabase database] reference];
    NSDictionary *values = [_categories valueForKey:_categories.allKeys[indexPath.section]];
    NSArray *prods = values.allValues[indexPath.row];

    [self.delegate addItemViewController:self didFinishEnteringItem:prods];
    [self.delegate addTitleCategory:self didFinishEnteringItem:_categories.allKeys[indexPath.section]];
    [self.delegate addSubtitleCategory:self didFinishEnteringItem:values.allKeys[indexPath.row]];
    [self.navigationController popViewControllerAnimated:YES];
}



@end
