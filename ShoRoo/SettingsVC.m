//
//  SettingsVC.m
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import "SettingsVC.h"
#import "SWRevealViewController.h"
@import Firebase;


@interface SettingsVC ()

@end

@implementation SettingsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        NSError *error;
        [[FIRAuth auth] signOut:&error];
        if (!error) {
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"uid"];
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"firstVC"];
            [self presentViewController:vc animated:YES completion:nil];
        }
    }
}
- (IBAction)menuAction:(id)sender {
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggleAnimated:YES];
}
@end
