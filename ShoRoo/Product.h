//
//  Product.h
//  
//
//  Created by Dmitry Kuzin on 10.10.16.
//
//

#import <Foundation/Foundation.h>
#import "ShowRoom.h"
@interface Product : NSObject

@property (nonatomic, copy) NSNumber * amount;
@property (nonatomic, copy) NSNumber * categoryId;

@property (nonatomic, copy) NSString * descr;
@property (nonatomic, copy) NSNumber * productId;

@property (nonatomic, copy) NSString * image;
@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSMutableDictionary * characteristics;


@property ShowRoom *showroom;

@end
