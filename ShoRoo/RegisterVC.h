//
//  RegisterVC.h
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterVC : UIViewController <UITextFieldDelegate>


@property (weak, nonatomic) IBOutlet UIView *nameView;

@property (weak, nonatomic) IBOutlet UIView *surnameView;
@property (weak, nonatomic) IBOutlet UIView *emailView;

@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *surnameField;
@property (weak, nonatomic) IBOutlet UITextField *emailField;

@property (weak, nonatomic) IBOutlet UITextField *passwordField;

- (IBAction)dismissAction:(id)sender;



- (IBAction)signUp:(id)sender;


@end
