//
//  MainVC.m
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import "MainVC.h"
#import "SWRevealViewController.h"
#import "MainProductCell.h"
#import "ShowProductVC.h"
@import Firebase;

@interface MainVC ()

@end

@implementation MainVC

- (void)viewDidLoad {
    [super viewDidLoad];
    FIRUser *user = [FIRAuth auth].currentUser;
    if (user != nil) {
        FIRDatabaseReference *ref = [[FIRDatabase database] reference];
        [[[ref child:@"mainView"] child:@"mainProduct"] observeEventType:FIRDataEventTypeValue withBlock:^(FIRDataSnapshot * _Nonnull snapshot) {
            self.mainTitle.text = [[snapshot childSnapshotForPath:@"title"] value];
            self.mainDescription.text = [[snapshot childSnapshotForPath:@"description"] value];
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                NSURL *url = [NSURL URLWithString:[[snapshot childSnapshotForPath:@"image"] value]];
                NSData *data = [[NSData alloc] initWithContentsOfURL:url];
                if (data == nil) self.mainImage.image = [UIImage imageNamed:@"Moon"];
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.mainImage.image = [UIImage imageWithData:data];
                });
            });
            self.products = [[NSMutableArray alloc] init];
            for (FIRDataSnapshot* currentProduct in [snapshot childSnapshotForPath:@"products"].children) {
                Product *product = [[Product alloc] init];
                product.title = [[currentProduct childSnapshotForPath:@"title"] value];
                product.image = [[currentProduct childSnapshotForPath:@"image"] value];
                product.productId = [[currentProduct childSnapshotForPath:@"id"] value];
                product.categoryId = [[currentProduct childSnapshotForPath:@"categoryId"] value];
                product.amount = [[currentProduct childSnapshotForPath:@"amount"] value];
                product.descr = [[currentProduct childSnapshotForPath:@"description"] value];
                product.characteristics = [[currentProduct childSnapshotForPath:@"characteristics"] value];
                ShowRoom * showroom = [[ShowRoom alloc] init];
                showroom.showroomId = [[[currentProduct childSnapshotForPath:@"showroom"] childSnapshotForPath:@"id"] value];
                showroom.image = [[[currentProduct childSnapshotForPath:@"showroom"] childSnapshotForPath:@"image"] value];
                showroom.name = [[[currentProduct childSnapshotForPath:@"showroom"] childSnapshotForPath:@"name"] value];
                product.showroom = showroom;

                [self.products addObject:product];
            }
            [self.collectionView reloadData];
        }];
    } else {
        UIViewController *vc = [[[UIStoryboard init] initWithString:@"Main"] instantiateViewControllerWithIdentifier:@"firstVC"];
        [self presentViewController:vc animated:YES completion:nil];
    }
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.products.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MainProductCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MainProductCell" forIndexPath:indexPath];
    Product *current = self.products[indexPath.row];
    cell.titleProduct.text = current.title;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSString *stringUrl = current.image;
        NSURL *url = [NSURL URLWithString:stringUrl];
        NSData *data = [[NSData alloc] initWithContentsOfURL:url];
        if (data == nil) cell.imageProduct.image = [UIImage imageNamed:@"Moon"];
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.imageProduct.image = [UIImage imageWithData:data];
        });
    });
    return cell;

}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    _selectedProduct = indexPath.row;
    [self performSegueWithIdentifier:@"showProduct" sender:nil];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqual: @"showProduct"]) {
        ShowProductVC *vc = (ShowProductVC *)segue.destinationViewController;
        vc.product = _products[_selectedProduct];
    }
}

- (IBAction)menuAction:(id)sender {
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggleAnimated:YES];
}
@end


