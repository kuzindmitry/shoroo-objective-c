//
//  AllCategoriesVC.h
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
@protocol SecondDelegate <NSObject>
- (void)addItemViewController:(id)controller didFinishEnteringItem:(NSArray *)item;
@end
*/

@class AllGlobalCategoriesVC;

@protocol CategoryDelegate <NSObject>
- (void)addItemViewController:(AllGlobalCategoriesVC *)controller didFinishEnteringItem:(NSArray *)item;
- (void)addTitleCategory:(AllGlobalCategoriesVC *)controller didFinishEnteringItem:(NSString *)title;
- (void)addSubtitleCategory:(AllGlobalCategoriesVC *)controller didFinishEnteringItem:(NSString *)subtitle;
@end

@interface AllGlobalCategoriesVC : UITableViewController

@property NSMutableDictionary *categories;
@property (nonatomic, weak) id <CategoryDelegate> delegate;
//@property (nonatomic, weak) id <SecondDelegate> delegate;

@end
