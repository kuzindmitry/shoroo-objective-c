//
//  CartVC.m
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import "CartVC.h"
#import "SWRevealViewController.h"

@interface CartVC ()

@end

@implementation CartVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (IBAction)menuAction:(id)sender {
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggleAnimated:YES];
}
@end
