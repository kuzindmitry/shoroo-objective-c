//
//  CategoryVC.m
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import "CategoryVC.h"
#import "AllGlobalCategoriesVC.h"
#import "SWRevealViewController.h"
#import "CategoryCell.h"
#import "ShowProductVC.h"
#import "Product.h"

@interface CategoryVC ()

@end

@implementation CategoryVC


-(void)viewWillAppear:(BOOL)animated {
    if (_products == nil && _isNotFirstResponder == NO) {
        AllGlobalCategoriesVC *allCategoryVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AllGlobalCategoriesVC"];
        allCategoryVC.delegate = self;
        _isNotFirstResponder = YES;
        [self.navigationController pushViewController:allCategoryVC animated:YES];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    CategoryCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProductCell" forIndexPath:indexPath];
    Product *product = _products[indexPath.row];
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSURL *url = [NSURL URLWithString:product.image];
        NSData *data = [[NSData alloc] initWithContentsOfURL:url];
        if (data == nil) cell.imageProduct.image = [UIImage imageNamed:@"Moon"];
        dispatch_async(dispatch_get_main_queue(), ^{
            cell.imageProduct.image = [UIImage imageWithData:data];
        });
    });
    
    cell.nameProduct.text = product.title;
    cell.descrProduct.text = product.descr;
    cell.amount.text = [NSString stringWithFormat:@"%@", product.amount];

    return cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _products.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = [[UIScreen mainScreen] bounds].size.width;
    width = (width-20)/2;
    return CGSizeMake(width, width*1.53);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedProduct = indexPath.row;
    [self performSegueWithIdentifier:@"showDetailProduct" sender:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"showDetailProduct"]) {
        ShowProductVC *vc = (ShowProductVC *)segue.destinationViewController;
        vc.product = _products[_selectedProduct];
    }
}

- (IBAction)menuAction:(id)sender {
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggleAnimated:YES];
}

- (void)addItemViewController:(AllGlobalCategoriesVC *)controller didFinishEnteringItem:(NSArray *)item {
    self.products = item;
    [self.collectionView reloadData];
}

- (void)addTitleCategory:(AllGlobalCategoriesVC *)controller didFinishEnteringItem:(NSString *)title {
    self.title = title;
}

- (void)addSubtitleCategory:(AllGlobalCategoriesVC *)controller didFinishEnteringItem:(NSString *)subtitle {
    self.subtitleLabel.text = subtitle;
}

- (IBAction)chooseCategory:(id)sender {
    AllGlobalCategoriesVC *allCategoryVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"AllGlobalCategoriesVC"];
    allCategoryVC.delegate = self;
    [self.navigationController pushViewController:allCategoryVC animated:YES];
}
@end
