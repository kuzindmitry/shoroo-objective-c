//
//  ProfileVC.h
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileVC : UIViewController <UIImagePickerControllerDelegate, UINavigationControllerDelegate>
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;

- (IBAction)menuAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *avatar;

- (IBAction)setImage:(id)sender;

@end
