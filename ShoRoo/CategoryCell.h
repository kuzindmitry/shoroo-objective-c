//
//  CategoryCell.h
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageProduct;
@property (weak, nonatomic) IBOutlet UILabel *nameProduct;
@property (weak, nonatomic) IBOutlet UILabel *descrProduct;
@property (weak, nonatomic) IBOutlet UILabel *amount;




@end
