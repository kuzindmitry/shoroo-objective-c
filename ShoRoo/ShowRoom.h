//
//  ShowRoom.h
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ShowRoom : NSObject

@property (nonatomic, copy) NSNumber * showroomId;
@property (nonatomic,copy) NSString * name;
@property (nonatomic,copy) NSString * image;
@end
