//
//  ProfileVC.m
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import "ProfileVC.h"
#import "SWRevealViewController.h"
@import Firebase;
@import FirebaseStorage;

@interface ProfileVC ()

@end

@implementation ProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    FIRUser *user = [FIRAuth auth].currentUser;
    if (user != nil) {
        _usernameLabel.text = user.displayName;
        _emailLabel.text = user.email;
        if (user.photoURL != nil) {
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSData *data = [[NSData alloc] initWithContentsOfURL:user.photoURL];
            if (data == nil) self.avatar.image = [UIImage imageNamed:@"ic_user"];
            dispatch_async(dispatch_get_main_queue(), ^{
                self.avatar.image = [UIImage imageWithData:data];
            });
        });
        } else {
            self.avatar.image = [UIImage imageNamed:@"ic_user"];
        }
    } else {
        UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"firstVC"];
        [self presentViewController:vc animated:YES completion:nil];
    }
    self.avatar.layer.cornerRadius = self.avatar.frame.size.width / 2;
    self.avatar.clipsToBounds = YES;
    // Do any additional setup after loading the view.
}




- (IBAction)menuAction:(id)sender {
    SWRevealViewController *revealViewController = self.revealViewController;
    [revealViewController revealToggleAnimated:YES];
}

- (IBAction)setImage:(id)sender {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    picker.editing = NO;
    picker.delegate = self;
    [self presentViewController:picker animated:YES completion:nil];
}


-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    FIRUser *user = [FIRAuth auth].currentUser;
    if (user != nil) {
    self.avatar.image = [info objectForKey:UIImagePickerControllerOriginalImage];
    FIRStorageReference *storageRef = [[FIRStorage storage] reference];
    // Create a reference to the file you want to upload
    FIRStorageReference *riversRef = [storageRef child:[NSString stringWithFormat:@"avatars/%@.jpg", user.uid]];
        NSData *data = UIImagePNGRepresentation([info objectForKey:UIImagePickerControllerOriginalImage]);
    // Upload the file to the path "images/rivers.jpg"
    [riversRef putData:data metadata:nil completion:^(FIRStorageMetadata *metadata, NSError *error) {
        if (error != nil) {
            // Uh-oh, an error occurred!
        } else {
            // Metadata contains file metadata such as size, content-type, and download URL.
            NSURL *downloadURL = metadata.downloadURL;
            FIRUserProfileChangeRequest *changeRequst = user.profileChangeRequest;
            changeRequst.photoURL = downloadURL;
            [changeRequst commitChangesWithCompletion:^(NSError * _Nullable error){
                if (error == nil) {
                    
                } else {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ошибка" message:[error localizedDescription] delegate:self cancelButtonTitle:@"Ок" otherButtonTitles:nil, nil];
                    [alert show];
                }
            }];
            
        }
    }];
    
    self.avatar.layer.cornerRadius = self.avatar.frame.size.width / 2;
    self.avatar.clipsToBounds = YES;
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
