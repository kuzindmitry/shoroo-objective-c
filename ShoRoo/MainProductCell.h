//
//  MainProductCell.h
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainProductCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageProduct;
@property (weak, nonatomic) IBOutlet UILabel *titleProduct;

@end
