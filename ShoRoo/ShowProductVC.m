//
//  ShowProductVC.m
//  ShoRoo
//
//  Created by Dmitry Kuzin on 11.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import "ShowProductVC.h"


@interface ShowProductVC ()

@end

@implementation ShowProductVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        NSURL *url = [NSURL URLWithString:_product.image];
        NSData *data = [[NSData alloc] initWithContentsOfURL:url];
        if (data == nil) self.imageProduct.image = [UIImage imageNamed:@"Moon"];
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imageProduct.image = [UIImage imageWithData:data];
        });
    });
    self.title = _product.title;
    self.titleLabel.text = _product.title;
    self.amountLabel.text = [NSString stringWithFormat:@"%@ руб.", self.product.amount];
    // Do any additional setup after loading the view.
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 300.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_segmentedControl.selectedSegmentIndex == 0) {
    
    TextViewShowProductCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TextViewShowProductCell" forIndexPath:indexPath];
    cell.descrTextView.text = _product.descr;
    
    return cell;
    } else if (_segmentedControl.selectedSegmentIndex == 1) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"characteristicsCell" forIndexPath:indexPath];
        NSDictionary *characteristics = _product.characteristics;
        cell.textLabel.text = characteristics.allKeys[indexPath.row];
        cell.detailTextLabel.text = [characteristics valueForKey:characteristics.allKeys[indexPath.row]];
        return cell;
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"showroomCellShowProduct" forIndexPath:indexPath];
        ShowRoom *showroom = _product.showroom;
        cell.textLabel.text = showroom.name;
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            NSURL *url = [NSURL URLWithString:showroom.image];
            NSData *data = [[NSData alloc] initWithContentsOfURL:url];
            if (data == nil) cell.imageView.image = [UIImage imageNamed:@"Moon"];
            dispatch_async(dispatch_get_main_queue(), ^{
                cell.imageView.image = [UIImage imageWithData:data];
            });
        });
        return cell;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_segmentedControl.selectedSegmentIndex == 1) {
        NSDictionary *characteristics = _product.characteristics;
            return characteristics.count;
    } else {
        return 1;
    }
}

- (IBAction)addToCart:(id)sender {
    
}

- (IBAction)minusCount:(id)sender {
    self.count -= 1;
    self.countLabel.text = [NSString stringWithFormat:@"%ld", (long)self.count];
    self.plusButton.enabled = YES;
    [self.plusButton setImage:[UIImage imageNamed:@"plus"] forState:UIControlStateNormal];
    if (self.count == 0) {
        self.minusButton.enabled = NO;
        [self.minusButton setImage:[UIImage imageNamed:@"minus"] forState:(UIControlStateNormal)];
        self.addButton.enabled = NO;
        [self.addButton setImage:[UIImage imageNamed:@"unactive-addtocart"] forState:UIControlStateNormal];
    }
}

- (IBAction)plusCount:(id)sender {
    self.minusButton.enabled = YES;
    [self.minusButton setImage:[UIImage imageNamed:@"active-minus"] forState:(UIControlStateNormal)];
    self.count += 1;
    self.countLabel.text = [NSString stringWithFormat:@"%ld", (long)self.count];
    self.addButton.enabled = YES;
    [self.addButton setImage:[UIImage imageNamed:@"active-addtocart"] forState:UIControlStateNormal];
    if (self.count == 5) {
        self.plusButton.enabled = NO;
        [self.plusButton setImage:[UIImage imageNamed:@"unactive-plus"] forState:UIControlStateNormal];
    }
}
- (IBAction)changeSegmentedControl:(id)sender {
    NSLog(@"%@", _product.characteristics);
    [self.tableView reloadData];
}

@end
