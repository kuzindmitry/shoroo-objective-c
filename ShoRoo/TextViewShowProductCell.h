//
//  TextViewShowProductCell.h
//  ShoRoo
//
//  Created by Dmitry Kuzin on 11.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextViewShowProductCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *descrTextView;


@end
