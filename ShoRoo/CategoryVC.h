//
//  CategoryVC.h
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AllGlobalCategoriesVC.h"

@interface CategoryVC : UIViewController <CategoryDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIRefreshControl *refreshControl;


- (IBAction)menuAction:(id)sender;

@property (weak, nonatomic) NSArray *products;

- (IBAction)chooseCategory:(id)sender;

@property BOOL isNotFirstResponder;
@property NSInteger selectedProduct;
@end
