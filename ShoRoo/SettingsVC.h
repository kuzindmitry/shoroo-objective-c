//
//  SettingsVC.h
//  ShoRoo
//
//  Created by Dmitry Kuzin on 10.10.16.
//  Copyright © 2016 Dmitry Kuzin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsVC : UITableViewController
- (IBAction)menuAction:(id)sender;

@end
